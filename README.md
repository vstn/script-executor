## Continuous JS- and TS- Script Executor (CSE)

* Executes JS-script [index.js](index.js) continuously on change.
* Compiles TS-script [index.ts](ts/index.ts) continuously on change to `ts/index.js` and executes it.

### Installation

```
sudo npm install -g gulp-cli@latest typescript@latest
```

### Start

```
npm start
```

## Licenses

* [`CSE`](LICENSE.md)
