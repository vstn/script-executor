'use strict';

const request = require('request');
const cheerio = require('cheerio');
const colors = require('colors');
/*
*	TEXT COLORS
*
*	black
*	red
*	green
*	yellow
*	blue
*	magenta
*	cyan
*	white
*	gray
*	grey
*/

/**
 * Entrants test
 */

module.exports.exec = function () {

	var obj = {
		a: 1,
		b: {
			x: 2,
			y: 3
		},
		c: {
			z: {
				q: 4
			}
		}
	};

	Object.prototype.random = function() {
		console.log('TODO: Object.prototype.random');
		/*
		*	TODO: implement this method
		*/
		return [];
	};

	Object.prototype.toRandomArray = function() {
		console.log('TODO: Object.prototype.toRandomArray');
		/*
		*	TODO: implement this method
		*/
		return [];
	};

	// TEST 1 - should return random object value
	const objRandom = obj.random();
	console.log('objRandom', objRandom);
	if (/[1-4]/.test(objRandom)) {
		console.log('TEST 1: obj.random() : CORRECT'.green);
	} else {
		console.log('TEST 1: obj.random() : INCORRECT'.red);
	}
	// TEST 2 - should return random values array permutation
	const randomArray = obj.toRandomArray();
	console.log('randomArray', randomArray);
	if (randomArray.includes(1) && randomArray.includes(2) && randomArray.includes(3) && randomArray.includes(4)) {
		console.log('TEST 1: obj.toRandomArray() : CORRECT'.green);
	} else {
		console.log('TEST 1: obj.toRandomArray() : INCORRECT'.red);
	}

	// TEST 1.1 - shound return undefined
	obj = {};
	const objRandom_1 = obj.random();
	if (typeof objRandom_1 === 'undefined') {
		console.log('TEST 1.1: obj.random() : CORRECT'.green);
	} else {
		console.log('TEST 1.1: obj.random() : INCORRECT'.red);
	}
	// TEST 2.1 - should return an empty array
	const randomArray_1 = obj.toRandomArray();
	if (Array.isArray(randomArray_1) && !randomArray_1.length) {
		console.log('TEST 1.1: obj.toRandomArray() : CORRECT'.green);
	} else {
		console.log('TEST 1.1: obj.toRandomArray() : INCORRECT'.red);
	}

	console.log('===');
	console.log('memory usage:', process.memoryUsage());
	console.log('execution time:', process.uptime());
};
